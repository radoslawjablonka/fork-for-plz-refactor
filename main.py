import os
import pytest
from typing import Union, List, NewType

Transactions = NewType("transactions", List[list])
Prices = NewType("prices", dict)


class IncorrectDataType(Exception):
    pass


def get_transactions(path: Union[str, os.PathLike]) -> Transactions:
    transactions = []
    with open(path, "r") as f:
        for line in f:
            transactions.append(line.split(","))

    return transactions


def get_prices(path: Union[str, os.PathLike]) -> dict:
    with open(path, "r") as f:
        prices_as_dict = eval(f.read())

    return prices_as_dict


def calc_total(transactions: Transactions, prices: Prices) -> int:
    total = 0
    for i in range(len(transactions)):
        _, share_name, quantity = transactions[i]
        total += int(quantity) * prices[share_name]
    return total


def calc_value(transactions: Transactions, total: int, prices: Prices) -> list:
    to_print = []
    for line in transactions:
        _, share_name, quantity = line
        quantity = quantity.strip()
        try:
            to_print.append(
                "%s  %.2f %.2f%%"
                % (
                    share_name,
                    prices[share_name] * int(quantity),
                    prices[share_name] * int(quantity) / total * 100,
                )
            )
        except:
            raise IncorrectDataType("correct your transactions or prices file")
    return to_print


def print_portfolio(
    trans_file: Union[str, os.PathLike] = "transactions.txt",
    price_file: Union[str, os.PathLike] = "prices.txt",
):
    """Print the stocks in the portfolio and their values"""
    transactions = get_transactions(trans_file)

    prices = get_prices(price_file)
    total = calc_total(transactions, prices)
    output_statements = calc_value(transactions, total, prices)

    print(*output_statements, sep="\n")
    print("Total %.2f 100.00%%" % total)
    return


# This is for pytest
def test_print_portfolio(capsys, tmpdir):
    """Test print_portfolio()"""
    trs_file = tmpdir.mkdir("one").join("portfolio-trs")
    trs_file.write("2023-01-03,AAPL,15\n")
    prices_file = tmpdir.mkdir("two").join("prices")
    prices_file.write('{"AAPL": 125.07}\n')
    print_portfolio(trs_file, prices_file)
    printed_output = capsys.readouterr().out
    expected_output = "AAPL  1876.05 100.00%\n" "Total 1876.05 100.00%\n"
    assert printed_output == expected_output


@pytest.fixture
def transaction():
    return [["2023-01-03", "AA", "15"]]


@pytest.fixture
def prices():
    return {"apple": 125.07}


def test_get_transactions(tmpdir, transaction):
    trs_file = tmpdir.mkdir("sub").join("trs.txt")
    trs_file.write(",".join(transaction[0]))
    assert get_transactions(trs_file) == transaction


def test_get_prices(tmpdir, prices):
    file = tmpdir.mkdir("sub").join("prices.txt")
    file.write(str(prices))
    assert get_prices(file) == prices


if __name__ == "__main__":
    print_portfolio()
